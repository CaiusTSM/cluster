# **README** #

This project is under the zLib license which is at the bottom of this read me file and in the License.txt file.

**How to setup the project:**

1. You need the Eclipse IDE standard version: [https://www.eclipse.org/downloads/](https://www.eclipse.org/downloads/)

2. You need git: [http://git-scm.com/downloads](http://git-scm.com/downloads)

```
If this is your first time using git you need to run these two commands (with "")
git config --global user.name "your_username"
git config --global user.email "you_email@site.com"
```

3. Create a new folder called "Cluster".

4. In the top right of this website there should be a HTTPS or SSH address left of the share button and right of the clone in source tree button: [http://i.gyazo.com/e3bc1eeb3f60388c0ff2edaa07e5f928.png](http://i.gyazo.com/e3bc1eeb3f60388c0ff2edaa07e5f928.png)

5. Set that address to HTTPS and copy it.

6. In the cluster folder right click and select "git bash" (windows) or open a console and change your directory to the cluster folder (linux / mac).

7. Type the following command (without "" and []): "git clone [the_address_you_copied_before]"

That cloned (copied / downloaded) the project from the server.
You should now have a folder inside the "Cluster" folder called "cluster" which should contain the project. **DO NOT MODIFY THE PROJECT YET! FIRST CREATE A NEW BRANCH!**

**How to create a new branch:**

1. Open git bash in the new folder that was created after cloning (make sure it is in the correct directory "Cluster/cluster", NOT "Cluster").

2. Type the following command (without "" and []): "git branch [your_username]"

3. Type the following command (without "" and []): "git checkout [your_username]"

Now you have created a new branch named your username and based on the "master" branch (master is like the trunk of the tree).
This new branch is now your workspace and you may modify whatever you want in that branch.
"git checkout ..." changed the current branch you are in (which branch you are viewing) to your branch (switched from master branch to your branch).

**How to post a new commit on my branch:**

After modifying your branch you can make a new "commit" which is basically a new "version" of your branch.
For example I am starting at version 0.0.0 (after getting my new branch setup), then I change some code and want to save that current version and upload it.
In order to do that I need make a new "commit".

1. Open git bash as done before.

2. Now before you commit you need to tell git which files and folders to include in the new version, and to do that you use "git add ...".

3. Type the following command (without "" and []): "git add -A" (the ".-A" option adds all files and folders that are new or modified or deleted, if you want to add a single file or folder you can do "git add [file_or_folder]")

4. After adding everything you want to commit type the following command (**without ' '**): 'git commit -m "Your message that is attached to this version, for example: I added a new cool item X"'

5. Now that you have created a new version, you can upload it to the server so that others may download it and branch off your branch. To do that use (without "" or []): "git push [the_address_you_copied_before] [your_branch_name]"

You have now created a new version and pushed it to the server for others to mess around with.
If you add "READY_FOR_USE" in your commit message then a admin (like myself) will take that version and make it part of the actual master branch.

**Using Eclipse**

1. Run Eclipse and when it asks you for a workspace select any folder EXCEPT "Cluster/cluster" (the project folder).

2. Now go to File->Import

3. Then in the new window that opened select General->Existing project into workspace

4. Then click Next and for the root directory click browse and select your "Cluster/cluster" folder.

5. Where it says "Projects:" make sure the cluster project is selected (should be the only project to select anyhow). Then click Finish.

6. In the left there is the package explorer window and in it should be the project "Cluster". Click the little arrow next to it to expand the project.

7. All the code is inside the "src" folder, and all the libraries are in the "libs" folder.

8. Click the little downward pointing arrow at the top right of the package explorer window (if you hover over it, it should say "View Menu"). [Image](http://i.gyazo.com/a0493d00e2c9feffae2d3016298cddb7.png)

9. Then select Package Presentation->Hierarchical (now all the packages in the project src folder are displayed nicely)

Now that you have the project setup in Eclipse you can start coding, but please follow the format of the files already in the project (look at the file "Cluster.java" in the "cluster" package).

**Code Format**

All member variables should be preceded with a underscore. For example: "private int _counter;"
This way it is clear that it is a member variable and you do not need to put "this." in front of it all the time.

"{" should be on its own line and "}" should be on its own line too.
For example:
```
public void setX(int x)
{
	_x = x;
}
```
A exception to this is when creating a array (int[] array = {...}).

functions / methods should start with lower case letters (as shown above).

Double space your lines (makes it more readable).

Look at Cluster.java in the cluster package for how to format.

In the "Cluster/cluster" folder there is a file called "Format.xml". Right click on your project in the package explorer and select Properties.
Then go to Java Code Style->Formatter and if it is not already set to "Format" then select import and select Format.xml. Then click ok.
Now when you press Ctrl->Shift->f it will auto format your code (it will not add underscores to your member variables though).

**Other:**

The master branch contains what is the actual game that others will download when they want to play it.

You can view other people's branched by doing a "git checkout [the_branch_you_want_to_view]".

Doing "git branch [new_branch_name]" will create a new branch from the current branch you are viewing. You can use this to take someone else's branch and branch from it.

More info on how to use git: [http://rogerdudler.github.io/git-guide/](http://rogerdudler.github.io/git-guide/)

"git add ." does not add deleted files.

**!DO NOT MODIFY A BRANCH THAT YOU DID NOT MAKE!** Instead branch from that branch and then modify stuff.

**License (zLib):**

Copyright (c) <2014> <Lucas Laukien>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.