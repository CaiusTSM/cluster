package physics;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

public class PhysicsBox
{
	public Body _body;
	
	public PhysicsBox(World world, BodyType bodyType, float width, float height, float density, float restitution, float friction)
	{
		PolygonShape shape = new PolygonShape();

		shape.setAsBox(width / 2.0f, height / 2.0f);

		FixtureDef fixtureDef = new FixtureDef();

		fixtureDef.shape = shape;

		fixtureDef.density = density;

		fixtureDef.restitution = restitution;

		fixtureDef.friction = friction;
		
		fixtureDef.isSensor = false;
		
		BodyDef bodyDef = new BodyDef();
		
		bodyDef.type = bodyType;
		
		_body = world.createBody(bodyDef);
		
		_body.createFixture(fixtureDef);
	}
	
	public PhysicsBox(World world, BodyType bodyType, float width, float height, float density, float restitution, float friction, boolean isSensor)
	{
		PolygonShape shape = new PolygonShape();

		shape.setAsBox(width / 2.0f, height / 2.0f);

		FixtureDef fixtureDef = new FixtureDef();

		fixtureDef.shape = shape;

		fixtureDef.density = density;

		fixtureDef.restitution = restitution;

		fixtureDef.friction = friction;

		fixtureDef.isSensor = isSensor;
		
		BodyDef bodyDef = new BodyDef();
		
		bodyDef.type = bodyType;
		
		_body = world.createBody(bodyDef);
		
		_body.createFixture(fixtureDef);
	}

	public PhysicsBox(World world, BodyType bodyType, float x, float y, float angle, float width, float height, float density, float restitution, float friction, boolean isSensor)
	{
		PolygonShape shape = new PolygonShape();

		shape.setAsBox(width / 2.0f, height / 2.0f);

		FixtureDef fixtureDef = new FixtureDef();

		fixtureDef.shape = shape;

		fixtureDef.density = density;

		fixtureDef.restitution = restitution;

		fixtureDef.friction = friction;

		fixtureDef.isSensor = isSensor;
		
		BodyDef bodyDef = new BodyDef();
		
		bodyDef.type = bodyType;
		
		_body = world.createBody(bodyDef);
		
		_body.createFixture(fixtureDef);
		
		_body.setTransform(new Vec2(x, y), angle);
	}
}
