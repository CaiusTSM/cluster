package physics;

import org.jbox2d.callbacks.DebugDraw;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Physics
{
	private World _world;
	
	private float _pixelsPerMeter;
	
	private boolean _debugDraw;
	
	public Physics(GameContainer container, float pixelsPerMeter, Vec2 worldGravity)
	{
		PhysicsDebugDraw debugDrawer = new PhysicsDebugDraw(container, pixelsPerMeter);
		
		debugDrawer.setFlags(DebugDraw.e_shapeBit | DebugDraw.e_jointBit);
		
		_world = new World(worldGravity);
		
		_world.setContactListener(new PhysicsListener());
		
		_world.setDebugDraw(debugDrawer);
		
		_pixelsPerMeter = pixelsPerMeter;
		
		_debugDraw = false;
	}
	
	public void render(Graphics graphics)
	{
		if (_debugDraw)
			_world.drawDebugData();
	}
	
	public void update(float delta, int velocityIterations, int positionIterations)
	{
		_world.step(delta, velocityIterations, positionIterations);
		
		_world.clearForces();
	}
	
	public void setDebugDraw(boolean drawDebug)
	{
		_debugDraw = drawDebug;
	}
	
	public World getWorld()
	{
		return _world;
	}
	
	public float getPixelsPerMeter()
	{
		return _pixelsPerMeter;
	}
}
