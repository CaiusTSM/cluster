package physics;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

public class PhysicsListener implements ContactListener
{
	@Override
	public void beginContact(Contact contact)
	{
		if (contact.m_fixtureA.isSensor())
			((SensorContact)contact.m_fixtureA.m_userData)._fixtures.add(contact.m_fixtureB);
		
		if (contact.m_fixtureB.isSensor())
			((SensorContact)contact.m_fixtureB.m_userData)._fixtures.add(contact.m_fixtureA);
	}

	@Override
	public void endContact(Contact contact)
	{
		if (contact.m_fixtureA.isSensor())
		{
			SensorContact sensor = ((SensorContact)contact.m_fixtureA.m_userData);
			
			if (sensor._fixtures.contains(contact.m_fixtureB))
				sensor._fixtures.remove(contact.m_fixtureB);
		}
		
		if (contact.m_fixtureB.isSensor())
		{
			SensorContact sensor = ((SensorContact)contact.m_fixtureB.m_userData);
			
			if (sensor._fixtures.contains(contact.m_fixtureA))
				sensor._fixtures.remove(contact.m_fixtureA);
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold manifold)
	{
	}
	
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse)
	{
	}
}