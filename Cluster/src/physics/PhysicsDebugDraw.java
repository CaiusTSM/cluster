package physics;

import org.jbox2d.callbacks.DebugDraw;
import org.jbox2d.common.Color3f;
import org.jbox2d.common.OBBViewportTransform;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;

public class PhysicsDebugDraw extends DebugDraw
{
	private Graphics _graphics;
	
	private float _pixelsPerMeter;
	
	public PhysicsDebugDraw(GameContainer container, float pixelsPerMeter)
	{
		super(new OBBViewportTransform());
        
		//viewportTransform.setExtents(container.getWidth() / 2, container.getHeight() / 2);
		
        _graphics = container.getGraphics();
        
        _pixelsPerMeter = pixelsPerMeter;
	}

	@Override
	public void drawCircle(Vec2 position, float radius, Color3f color)
	{
		_graphics.setColor(new Color(color.x, color.y, color.z, 0.5f));
		
		_graphics.drawOval((position.x - radius) * _pixelsPerMeter, (position.y - radius) * _pixelsPerMeter, radius, radius);
	}

	@Override
	public void drawPoint(Vec2 position, float radius, Color3f color)
	{
		_graphics.setColor(new Color(color.x, color.y, color.z, 0.5f));
		
		_graphics.drawOval((position.x - radius) * _pixelsPerMeter, (position.y - radius) * _pixelsPerMeter, radius, radius);
	}

	@Override
	public void drawSegment(Vec2 pointA, Vec2 pointB, Color3f color)
	{
		_graphics.setColor(new Color(color.x, color.y, color.z, 0.5f));
		
		_graphics.drawLine(pointA.x * _pixelsPerMeter, pointA.y * _pixelsPerMeter, pointB.x * _pixelsPerMeter, pointB.y * _pixelsPerMeter);
	}

	@Override
	public void drawSolidCircle(Vec2 position, float radius, Vec2 axis, Color3f color)
	{
		_graphics.setColor(new Color(color.x, color.y, color.z, 0.5f));
		
		_graphics.fillOval((position.x - radius) * _pixelsPerMeter, (position.y - radius) * _pixelsPerMeter, radius, radius);
		
		_graphics.setColor(_graphics.getColor().brighter(1.5f));
		
		_graphics.drawLine((position.x - radius) * _pixelsPerMeter, (position.y - radius) * _pixelsPerMeter, (axis.x - radius) * _pixelsPerMeter, (axis.y - radius) * _pixelsPerMeter);
	}

	@Override
	public void drawSolidPolygon(Vec2[] vertices, int numVerts, Color3f color)
	{
		_graphics.setColor(new Color(color.x * 0.7f, color.y * 0.7f, color.z * 0.7f, 0.5f));
		
		Polygon polygon = new Polygon();
		
		for (int i = 0; i < numVerts; i++)
			polygon.addPoint(vertices[i].x * _pixelsPerMeter, vertices[i].y * _pixelsPerMeter);
		
		_graphics.fill(polygon);
		
		_graphics.setColor(_graphics.getColor().brighter(0.5f));
		
		_graphics.draw(polygon);
	}

	@Override
	public void drawString(float x, float y, String text, Color3f color)
	{
		_graphics.setColor(new Color(color.x, color.y, color.z, 0.5f));
		
		_graphics.drawString(text, x * _pixelsPerMeter, y * _pixelsPerMeter);
	}

	@Override
	public void drawTransform(Transform transform)
	{
	}
}