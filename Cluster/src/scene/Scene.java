package scene;

import java.util.ArrayList;
import java.util.HashMap;

import org.newdawn.slick.Graphics;

public class Scene
{
	private ArrayList<SceneObject> _sceneObjects;

	private HashMap<String, ArrayList<SceneObject>> _named;

	private ArrayList<ArrayList<SceneObject>> _renderLayers;

	public Scene()
	{
		_sceneObjects = new ArrayList<SceneObject>(4);

		_named = new HashMap<String, ArrayList<SceneObject>>();

		_renderLayers = new ArrayList<ArrayList<SceneObject>>(4);

		_renderLayers.add(new ArrayList<SceneObject>(4));
	}

	public void setNumRenderLayers(int numRenderLayers)
	{
		if (_renderLayers.size() < numRenderLayers)
			while (numRenderLayers - _renderLayers.size() > 0)
				_renderLayers.add(new ArrayList<SceneObject>(4));
		else
			while (_renderLayers.size() > numRenderLayers)
				_renderLayers.remove(_renderLayers.size() - 1);
	}

	public void add(SceneObject sceneObject)
	{
		String name = sceneObject.getName();

		if (!name.isEmpty())
		{
			if (!_named.containsKey(name))
				_named.put(name, new ArrayList<SceneObject>());

			_named.get(name).add(sceneObject);
		}

		if (sceneObject.getRenderLayer() != -1)
			_renderLayers.get(sceneObject.getRenderLayer()).add(sceneObject);

		_sceneObjects.add(sceneObject);

		sceneObject.onAdd();
	}

	public void remove(SceneObject sceneObject)
	{
		sceneObject.onRemove();

		if (!sceneObject.getName().isEmpty())
			_named.remove(sceneObject.getName());

		if (sceneObject.getRenderLayer() != -1)
			_renderLayers.get(sceneObject.getRenderLayer()).remove(sceneObject);

		_sceneObjects.remove(sceneObject);
	}

	public void switchRenderLayer(SceneObject sceneObject, int newRenderLayer)
	{
		if (sceneObject.getRenderLayer() != -1)
			_renderLayers.get(sceneObject.getRenderLayer()).remove(sceneObject);

		if (newRenderLayer != -1)
			_renderLayers.get(newRenderLayer).add(sceneObject);
	}

	public void update(float delta)
	{
		for (int i = 0; i < _sceneObjects.size(); i++)
			_sceneObjects.get(i).update(delta);
	}

	public void render(Graphics graphics)
	{
		for (int i = 0; i < _renderLayers.size(); i++)
			for (int j = 0; j < _renderLayers.get(i).size(); j++)
				_renderLayers.get(i).get(j).render(graphics);
	}

	public SceneObject get(String name)
	{
		if (_named.containsKey(name))
			return _named.get(name).get(0);

		return null;
	}

	public ArrayList<SceneObject> getAll(String name)
	{
		if (_named.containsKey(name))
			return _named.get(name);

		return null;
	}

	public int numRenderLayers()
	{
		return _renderLayers.size();
	}
}
