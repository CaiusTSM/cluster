package scene.objects;

import graphics.Camera;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

import physics.Physics;
import physics.SensorContact;
import resources.Resources;
import scene.Scene;
import scene.SceneObject;

public class Player extends SceneObject
{
	private Input _input;
	
	private Camera _camera;
	
	private Image _image;
	
	private World _world;
	
	private float _pixelsPerMeter;
	
	private FixtureDef _topFixtureDef;
	
	private FixtureDef _botFixtureDef;
	
	private FixtureDef _jumpSensorFixtureDef;
	
	private SensorContact _jumpSensor;
	
	private BodyDef _bodyDef;
	
	private Body _body;
	
	private Vec2 _tempVec;
	
	public Player(Scene scene, String name, int renderLayer, Input input, Physics physics, Camera camera)
	{
		super(scene, name, renderLayer);
		
		_input = input;
		
		_camera = camera;
		
		_image = (Image)Resources.getResource("/images/player/Player.png");
		
		_image.setFilter(Image.FILTER_NEAREST);
		
		_world = physics.getWorld();
		
		_pixelsPerMeter = physics.getPixelsPerMeter();
		
		PolygonShape topShape = new PolygonShape();
		
		topShape.set(new Vec2[] {new Vec2(-0.35f, -0.5f), new Vec2(-0.5f, -0.35f), new Vec2(-0.5f, 0.38f), new Vec2(0.5f, 0.38f), new Vec2(0.5f, -0.35f), new Vec2(0.35f, -0.5f)}, 6);
		
		_topFixtureDef = new FixtureDef();
		
		_topFixtureDef.shape = topShape;
		
		_topFixtureDef.density = 1.0f;
		
		_topFixtureDef.restitution = 0.005f;
		
		_topFixtureDef.friction = 0.01f;
		
		PolygonShape botShape = new PolygonShape();
		
		botShape.set(new Vec2[] {new Vec2(-0.35f, 0.5f), new Vec2(0.35f, 0.5f), new Vec2(0.5f, 0.4f), new Vec2(-0.5f, 0.4f)}, 4);
		
		_botFixtureDef = new FixtureDef();
		
		_botFixtureDef.shape = botShape;
		
		_botFixtureDef.density = 1.0f;
		
		_botFixtureDef.restitution = 0.005f;
		
		_botFixtureDef.friction = 0.01f;
		
		PolygonShape jumpSensorShape = new PolygonShape();
		
		jumpSensorShape.set(new Vec2[] {new Vec2(-0.35f, 0.5f), new Vec2(0.35f, 0.5f), new Vec2(0.35f, 0.6f), new Vec2(-0.35f, 0.6f)}, 4);
		
		_jumpSensorFixtureDef = new FixtureDef();
		
		_jumpSensorFixtureDef.shape = jumpSensorShape;
		
		_jumpSensorFixtureDef.density = 0.0f;
		
		_jumpSensorFixtureDef.restitution = 0.05f;
		
		_jumpSensorFixtureDef.friction = 0.1f;
		
		_jumpSensorFixtureDef.isSensor = true;
		
		_jumpSensor = new SensorContact();
		
		_jumpSensorFixtureDef.userData = _jumpSensor;
		
		_bodyDef = new BodyDef();
		
		_bodyDef.type = BodyType.DYNAMIC;
		
		_bodyDef.fixedRotation = true;
		
		_tempVec = new Vec2();
	}
	
	@Override
	public void onAdd()
	{
		_body = _world.createBody(_bodyDef);
		
		_body.createFixture(_topFixtureDef);
		
		_body.createFixture(_botFixtureDef);
		
		_body.createFixture(_jumpSensorFixtureDef);
	}
	
	@Override
	public void render(Graphics graphics)
	{
		graphics.drawImage(_image, _body.getWorldCenter().x * _pixelsPerMeter - _image.getWidth() / 2.0f, _body.getWorldCenter().y * _pixelsPerMeter - _image.getHeight() / 2.0f);
	}
	
	@Override
	public void update(float delta)
	{
		if (_input.isKeyDown(Input.KEY_RIGHT))
		{
			if (_jumpSensor._fixtures.size() != 0)
			{
				if (_body.getLinearVelocity().x < 0.0f)
				{
					_tempVec.x = 0.0f;
					
					_tempVec.y = 0.0f;
					
					_body.setLinearVelocity(_tempVec);
				}
				
				_tempVec.x = 0.2f;
			}
			else
				_tempVec.x = 0.1f;
			
			_tempVec.y = 0.0f;
			
			_body.applyLinearImpulse(_tempVec, _body.getWorldCenter());
		}
		else if (_input.isKeyDown(Input.KEY_LEFT))
		{
			if (_jumpSensor._fixtures.size() != 0)
			{
				if (_body.getLinearVelocity().x > 0.0f)
				{
					_tempVec.x = 0.0f;
					
					_tempVec.y = 0.0f;
					
					_body.setLinearVelocity(_tempVec);
				}
				
				_tempVec.x = -0.2f;
			}
			else
				_tempVec.x = -0.1f;
			
			_tempVec.y = 0.0f;
			
			_body.applyLinearImpulse(_tempVec, _body.getWorldCenter());
		}
		else
		{
			if (_jumpSensor._fixtures.size() != 0)
				_tempVec.x = 0.0f;
			else
				_tempVec.x = _body.getLinearVelocity().x * 0.99f;
			
			_tempVec.y = _body.getLinearVelocity().y;
			
			_body.setLinearVelocity(_tempVec);
		}
		
		if (_body.getLinearVelocity().x > 6.0f)
			_body.getLinearVelocity().x = 6.0f;
		else if (_body.getLinearVelocity().x < -6.0f)
			_body.getLinearVelocity().x = -6.0f;
		
		if (_input.isKeyDown(Input.KEY_SPACE) && _jumpSensor._fixtures.size() != 0)
		{
			_tempVec.x = _body.getLinearVelocity().x;
			
			_tempVec.y = -7.0f;
			
			_body.setLinearVelocity(_tempVec);
		}
		
		_camera._x = _body.getWorldCenter().x * _pixelsPerMeter;
		
		_camera._y = _body.getWorldCenter().y * _pixelsPerMeter;
	}
	
	@Override
	public void onRemove()
	{
	}
}
