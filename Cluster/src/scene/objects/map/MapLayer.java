package scene.objects.map;

import org.newdawn.slick.Graphics;

import scene.Scene;
import scene.SceneObject;

public class MapLayer extends SceneObject
{
	public MapLayer(Scene scene, String name, int renderLayer)
	{
		super(scene, name, renderLayer);
	}
	
	@Override
	public void onAdd()
	{
	}
	
	@Override
	public void render(Graphics graphics)
	{
	}
}
