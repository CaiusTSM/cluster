package scene.objects.gamestates;

import graphics.Camera;

import org.newdawn.slick.GameContainer;

import physics.Physics;
import resources.Resources;
import resources.loaders.ImageLoader;
import scene.Scene;

public class LoadingState extends GameState
{
	private Physics _physics;
	
	private Camera _camera;
	
	public LoadingState(Scene scene, String name, int renderLayer, GameContainer container, Physics physics, Camera camera)
	{
		super(scene, name, renderLayer, container);
		
		_physics = physics;
		
		_camera = camera;
	}

	@Override
	public void onAdd()
	{
		// load stuff here
		
		ImageLoader imageLoader = new ImageLoader();
		
		Resources.load(imageLoader, "/images/player/Player.png");
		
		_scene.remove(this);
		
		_scene.add(new InGameState(_scene, "InGameState", -1, _container, _physics, _camera));
	}
}
