package scene.objects.gamestates;

import org.newdawn.slick.GameContainer;

import scene.Scene;
import scene.SceneObject;

public class GameState extends SceneObject
{
	public GameContainer _container;
	
	public GameState(Scene scene, String name, int renderLayer, GameContainer container)
	{
		super(scene, name, renderLayer);
		
		_container = container;
	}
}