package scene.objects.gamestates;

import graphics.Camera;

import org.jbox2d.dynamics.BodyType;
import org.newdawn.slick.GameContainer;

import physics.Physics;
import physics.PhysicsBox;
import scene.Scene;
import scene.objects.Player;

public class InGameState extends GameState
{
	private Physics _physics;
	
	private Camera _camera;
	
	public InGameState(Scene scene, String name, int renderLayer, GameContainer container, Physics physics, Camera camera)
	{
		super(scene, name, renderLayer, container);
		
		_physics = physics;
		
		_camera = camera;
	}

	@Override
	public void onAdd()
	{
		//spawning physics for testing
		
		new PhysicsBox(_physics.getWorld(), BodyType.STATIC, 0.0f, (_container.getHeight() / 3.0f) / _physics.getPixelsPerMeter(), 0.0f, _container.getWidth() / _physics.getPixelsPerMeter(), 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		new PhysicsBox(_physics.getWorld(), BodyType.DYNAMIC, 1.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		new PhysicsBox(_physics.getWorld(), BodyType.DYNAMIC, -1.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		new PhysicsBox(_physics.getWorld(), BodyType.DYNAMIC, 1.5f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		new PhysicsBox(_physics.getWorld(), BodyType.DYNAMIC, -1.5f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		new PhysicsBox(_physics.getWorld(), BodyType.DYNAMIC, 1.5f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		new PhysicsBox(_physics.getWorld(), BodyType.DYNAMIC, -1.5f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.1f, 0.95f, false);
		
		//spawn player
		
		_scene.add(new Player(_scene, "Player", 0, _container.getInput(), _physics, _camera));
	}
}
