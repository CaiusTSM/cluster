package scene;

import org.newdawn.slick.Graphics;

public class SceneObject
{
	private String _name;
	
	private int _renderLayer;
	
	protected Scene _scene;
	
	public SceneObject(Scene scene, String name, int renderLayer)
	{
		_name = name;
		
		_scene = scene;
		
		_renderLayer = renderLayer;
	}
	
	public void onAdd()
	{
	}
	
	public void update(float delta)
	{
	}
	
	public void render(Graphics graphics)
	{
	}
	
	public void onRemove()
	{
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getRenderLayer()
	{
		return _renderLayer;
	}
}
