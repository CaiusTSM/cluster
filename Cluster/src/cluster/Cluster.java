package cluster;

import graphics.Camera;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.jbox2d.common.Vec2;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import physics.Physics;
import resources.Resources;
import scene.Scene;
import scene.objects.gamestates.LoadingState;

public class Cluster extends BasicGame
{
	private JSONObject _config;
	
	private float _timeStep;
	
	private int _maxNumSteps;
	
	private float _timePassed;
	
	private Camera _camera;
	
	private Physics _physics;
	
	private int _velocityIterations;
	
	private int _positionIterations;
	
	private Scene _scene;
	
	public Cluster(String title, JSONObject config)
	{
		super(title);
		
		_config = config;
	}
	
	@Override
	public void init(GameContainer container) throws SlickException
	{
		Resources.create((String)_config.get("ResourcePath"));
		
		_timeStep = 0.017f;
		
		_maxNumSteps = 10;
		
		_timePassed = 0.0f;
		
		_camera = new Camera();
		
		_physics = new Physics(container, 64.0f, new Vec2(0.0f, 10.0f));
		
		_physics.setDebugDraw(true);
		
		_velocityIterations = 10;
		
		_positionIterations = 10;
		
		_scene = new Scene();
		
		LoadingState loadingState = new LoadingState(_scene, "GameState", -1, container, _physics, _camera);
		
		_scene.add(loadingState);
		
		container.getGraphics().setBackground(Color.gray);
	}
	
	@Override
	public void render(GameContainer container, Graphics graphics) throws SlickException
	{		
		graphics.pushTransform();
		
		graphics.translate(container.getWidth() / 2.0f, container.getHeight() / 2.0f);
		
		_camera.pushTransform(graphics);
		
		_scene.render(graphics);
		
		_physics.render(graphics);
		
		_camera.popTransform(graphics);
		
		graphics.popTransform();
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		float dt = (float)delta / 1000.0f;
		
		_timePassed += dt;
		
		int numSteps = 0;
		
		while (_timePassed >= _timeStep && numSteps < _maxNumSteps)
		{
			_physics.update(_timeStep, _velocityIterations, _positionIterations);
				
			_scene.update(_timeStep);
			
			_timePassed -= _timeStep;
			
			numSteps++;
			
			if (numSteps == _maxNumSteps)
				_timePassed = 0.0f;
		}
	}
	
	public static void main(String[] args)
	{
		//the configuration object
		
		JSONObject config = null;
		
		//parse configuration file (config.json)
		
		try
		{
			String data = "";
			
			BufferedReader reader = new BufferedReader(new FileReader(new File("config.json")));
			
			String line = "";
			
			while ((line = reader.readLine()) != null)
				data += line + "\n";
			
			reader.close();
			
			JSONParser parser = new JSONParser();
			
			config = (JSONObject)parser.parse(data);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//end of configuration parsing
		
		System.setProperty("java.library.path", "libs");
		
		System.setProperty("org.lwjgl.librarypath", new File("libs/natives/" + config.get("OS")).getAbsolutePath());
		
		try
		{	
			//start the game
			
			AppGameContainer container;
			
			container = new AppGameContainer(new Cluster("Cluster", config));
			
			container.setDisplayMode((int)((long)config.get("Width")), (int)((long)config.get("Height")), false);
			
			container.setAlwaysRender(true);
			
			container.setTargetFrameRate(60);
			
			//v-sync option
			
			if (config.get("VSync").equals(true))
				container.setVSync(true);
			else
				container.setVSync(false);
			
			//show FPS in top left corner of window option
			
			if (config.get("ShowFPS").equals(true))
				container.setShowFPS(true);
			else
				container.setShowFPS(false);
			
			container.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}