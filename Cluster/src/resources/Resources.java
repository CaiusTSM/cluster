package resources;

import java.util.HashMap;

public class Resources
{
	private static Resources _resources = null;
	
	private String _resourcesPath;
	
	private HashMap<String, Object> _loaded;
	
	private Resources(String resourcesPath)
	{
		_resourcesPath = resourcesPath;
		
		_loaded = new HashMap<String, Object>();
	}
	
	public static void create(String resourcesPath)
	{
		_resources = new Resources(resourcesPath);
	}
	
	public static void destroy()
	{
		_resources = null;
	}
	
	public static void load(ResourceLoader loader, String filePath)
	{
		Object resource = loader.load(_resources._resourcesPath + filePath);
		
		if (resource == null)
			throw new RuntimeException();
		else if (!_resources._loaded.containsKey(filePath))
			_resources._loaded.put(filePath, resource);
	}
	
	public static Object getResource(String filePath)
	{
		return _resources._loaded.get(filePath);
	}
	
	public static void unLoad(String file)
	{
		_resources._loaded.remove(file);
	}
}
