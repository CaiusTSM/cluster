package resources.loaders;

import org.newdawn.slick.Image;

import resources.ResourceLoader;

public class ImageLoader extends ResourceLoader
{
	@Override
	public Object load(String filePath)
	{
		Image image = null;
		
		try
		{
			image = new Image(filePath);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return image;
	}
}
