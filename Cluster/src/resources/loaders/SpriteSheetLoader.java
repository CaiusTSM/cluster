package resources.loaders;

import java.io.File;
import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.newdawn.slick.SpriteSheet;

import resources.ResourceLoader;

public class SpriteSheetLoader extends ResourceLoader
{
	@Override
	public Object load(String filePath)
	{
		SpriteSheet spriteSheet = null;
		
		String[] split = filePath.split("/");
		
		String fileName = split[split.length - 1];
		
		String spriteSheetDataFile = filePath.replace(fileName, fileName.split("\\.")[0] + ".json");
		
		try
		{
			FileReader reader = new FileReader(new File(spriteSheetDataFile));
			
			JSONParser parser = new JSONParser();
			
			JSONObject data = (JSONObject)parser.parse(reader);
			
			spriteSheet = new SpriteSheet(filePath, (int)(long)(data.get("SpriteWidth")), (int)(long)data.get("SpriteHeight"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return spriteSheet;
	}
}
