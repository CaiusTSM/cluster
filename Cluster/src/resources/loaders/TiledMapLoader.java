package resources.loaders;

import org.newdawn.slick.tiled.TiledMap;

import resources.ResourceLoader;

public class TiledMapLoader extends ResourceLoader
{
	@Override
	public Object load(String filePath)
	{
		TiledMap tiledMap = null;
		
		try
		{
			tiledMap = new TiledMap(filePath);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return tiledMap;
	}
}
