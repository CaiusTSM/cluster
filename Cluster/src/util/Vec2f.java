package util;

public class Vec2f
{
	public float _x;
	
	public float _y;
	
	public Vec2f()
	{
		_x = 0.0f;
		
		_y = 0.0f;
	}
	
	public Vec2f(float x, float y)
	{
		_x = x;
		
		_y = y;
	}
	
	public Vec2f(Vec2f clone)
	{
		_x = clone._x;
		
		_y = clone._y;
	}
	
	public void set(Vec2f other)
	{
		_x = other._x;
		
		_y = other._y;
	}
}
