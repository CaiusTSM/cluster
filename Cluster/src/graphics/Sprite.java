package graphics;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import util.Vec2f;

public class Sprite
{
	private Image _image;
	
	private Vec2f _position;
	
	private Vec2f _origin;
	
	private float _angle;
	
	public Sprite(Image image)
	{
		_image = image;
		
		_origin = new Vec2f();
		
		_position = new Vec2f();
		
		_angle = 0.0f;
	}
	
	public void setPosition(float x, float y)
	{
		_position._x = x;
		
		_position._y = y;
	}
	
	public void setPosition(Vec2f position)
	{
		_position.set(position);
	}
	
	public void setOrigin(float x, float y)
	{
		_origin._x = x;
		
		_origin._y = y;
	}
	
	public void setOrigin(Vec2f origin)
	{
		_origin.set(origin);
	}
	
	public void setAngle(float angle)
	{
		_angle = angle;
	}
	
	public void render(Graphics graphics)
	{
		graphics.pushTransform();
		
		graphics.translate(_position._x, _position._y);
		
		graphics.rotate(_origin._x, _origin._y, _angle);
		
		graphics.drawImage(_image, 0.0f, 0.0f);
		
		graphics.popTransform();
	}
}
