package graphics;

import org.newdawn.slick.Graphics;

public class Camera
{
	public float _x;
	
	public float _y;
	
	public float _scaleX;
	
	public float _scaleY;
	
	public Camera()
	{
		_x = 0.0f;
		
		_y = 0.0f;
		
		_scaleX = 1.0f;
		
		_scaleY = 1.0f;
	}
	
	public void pushTransform(Graphics graphics)
	{
		graphics.pushTransform();
		
		graphics.translate(-_x, -_y);
		
		graphics.scale(_scaleX, _scaleY);
	}
	
	public void popTransform(Graphics graphics)
	{
		graphics.popTransform();
	}
}
